const path = require('path');
const webpack = require('webpack');
const HWP = require('html-webpack-plugin');

let config = {
  mode: 'development',
  node: {
    __dirname: true,
    __filename: true
  },
  entry: {
    index: path.resolve(__dirname, '') + '/index.jsx'
  },
  plugins: [
    new HWP({
      title: "Exercise CRUD",
      template: "index.ejs",
      inject: "body",
      filename: "../index.html"
    })
  ],
  resolve: {
    extensions: ['.js','.jsx'],
    alias: {
      Container: path.resolve(__dirname, 'Component') + '/Container',
      Presentation: path.resolve(__dirname, 'Component') + '/Presentational',
      HOC: path.resolve(__dirname, 'Component') + '/HOC',
      Utils: path.resolve(__dirname,'Utils'),
      Settings: path.resolve(__dirname,'Settings'),
      Store: path.resolve(__dirname,'Store'),
      Style: path.resolve(__dirname,'Style'),
    }
  },
  output: {
    publicPath: '/dist/'
  },
  module: {
    rules: [
      {
        test: /\.js(x)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['babel-preset-env','babel-preset-react'],
            plugins: [
              require('babel-plugin-transform-runtime')
            ]
          }
        }
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: ['style-loader','css-loader']
      }
    ]
  }

};

module.exports = config;
