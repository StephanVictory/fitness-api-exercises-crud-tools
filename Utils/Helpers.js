const bindFunctions = (scope, moduleFunctions) => {
  let moduleIdentifiers = Object.keys(moduleFunctions);
  Object.values(moduleFunctions)
    .map((func, identifier) =>
      scope[moduleIdentifiers[identifier]] = func.bind(scope));
};

function autobindFunctions(scope, moduleGroup) {
  if (Array.isArray(moduleGroup)) {
    moduleGroup.map(group => bindFunctions(scope, group));
  } else {
    bindFunctions(scope, moduleGroup);
  }
}


export
{
  autobindFunctions
};
