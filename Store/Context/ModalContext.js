import {createContext} from 'react';

let modalContextData = {};
let {Provider, Consumer} = createContext(modalContextData);

export {Provider, Consumer};
