import React from 'react';
import './Style/default.css';
import TopBar from 'Container/Element/Bar/TopBar';
import MainMenu from 'Container/Element/Menu/MainMenu';
export default class Home extends React.Component{
  constructor(props){
    super(props);
  }

  render(){
    return <div className='home'>
      <TopBar/>
      <div className='home-content'>
        <MainMenu/>
      </div>
    </div>;
  }
}
