import React from 'react';
import './Style/default.css';
const sizes = {
  xs:'button-xs',
  sm:'button-sm',
  md:'button-md',
  lg:'button-lg',
  xl:'button-xl',
}

const colors = {
  green:'bg-green',
  cyan:'bg-cyan',
  crimson:'bg-crimson',
  orange:'bg-orange'
}

export default function Button(props) {
  let {text='',size = 'xs', color = 'green', onClick = () => alert('Clicked')} = props;
  return <input
    type='button'
    className={`button ${sizes[size]} ${colors[color]}`}
    value={text}
    onClick={onClick}/>
}
