import React from 'react';
import './Style/default.css';

export default function CloseButton({onClick,classes=''}){
  return <div className={`cross ${classes}`} onClick={onClick}>
    <div className="line forward"></div>
    <div className="line backward"></div>
  </div>;
}
