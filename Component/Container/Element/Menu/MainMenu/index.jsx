import React from 'react';
import './Style/default.css';
import Methods from './Methods';
import withModalContext from 'HOC/withModalContext';
import {autobindFunctions} from 'Utils/Helpers';
import {Consumer} from 'Store/Context/ModalContext';
import Button from 'Container/Element/Button';
import Modal from 'Presentation/Element/Modal';

class MainMenu extends React.Component {
  constructor(props){
    super(props);
    this.state = {};
    autobindFunctions(this,Methods);
  }

  render(){
    return <Consumer>
      { ({toggle,state}) =>
      <div className='main-menu'>
        <Modal toggle={toggle} {...state}/>
        <div className='main-menu-button-row'>
          <Button text='Add New' size='lg' color='green'
            onClick={e => toggle('NewEntry', 'New Entry')}/>
          <Button text='Browse Library' size='lg' color='orange'
            onClick={e => toggle('BrowseEntries')}/>
        </div>
        <div className='main-menu-button-row'>
          <Button text='Update Library' size='lg' color='cyan'
            onClick={e => toggle('UpdateEntry')}/>
          <Button text='Delete Registry' size='lg' color='crimson'
            onClick={e => toggle('DeleteEntry')}/>
        </div>
      </div>
      }
    </Consumer>;
  }
}

export default withModalContext(MainMenu);
