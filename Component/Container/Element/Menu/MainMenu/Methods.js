function openModal(selectedModal){
  switch(selectedModal){
    case 'new':
        this.openAddNewModal();
        break;
    case 'browse':

        break;
    case 'update':

        break;
    case 'delete':

        break;
  }
}

function closeModal(selectedModal){
  switch(selectedModal){
    case 'new':
        this.closeAddNewModal();
        break;
    case 'browse':
        this.openAddNewModal();
        break;
    case 'update':
        this.openAddNewModal();
        break;
    case 'delete':
        this.openAddNewModal();
        break;
  }
}

function openAddNewModal(){
  this.setState({isAddNewOpen: true});
}

function closeAddNewModal(){
  this.setState({isAddNewOpen: false});
}

function onChangeAdditionSelection(selectedAdditionType){
  if(selectedAdditionType == 'exercise'){
    this.setState({newExerciseSelected:true,newExerciseTypeSelected:false});
  }else{
    this.setState({newExerciseSelected:false,newExerciseTypeSelected:true});
  }
}

async function setModalExerciseTypes(){
  let exerciseTypes = await this.getExerciseTypes();

  this.setState({exerciseTypes:exerciseTypes});
}

export default {
  openModal,
  openAddNewModal,
  onChangeAdditionSelection,
  closeAddNewModal,
  setModalExerciseTypes
};
