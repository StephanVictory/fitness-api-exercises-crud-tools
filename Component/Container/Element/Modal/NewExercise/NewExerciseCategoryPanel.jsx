import React from 'react';

export default function NewExerciseCategoryPanel(props){
  let {setState,newExerciseCategoryName = ''} = props;
  return  <div className='new-exercise-category-modal-entry'>
      <div>
        <label>Exercise Category name:</label>
        <input type='text' value={newExerciseCategoryName}
          onChange={e => setState({newExerciseCategoryName:e.target.value})}/>
      </div>
  </div>;
}
