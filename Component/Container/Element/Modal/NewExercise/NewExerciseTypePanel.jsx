import React from 'react';

export default function NewExerciseTypePanel(props){
  let {setState,newExerciseTypeName = ''} = props;
  return  <div className='new-exercise-type-modal-entry'>
      <div>
        <label>Exercise Type name:</label>
        <input type='text' value={newExerciseTypeName}
          onChange={e => setState({newExerciseTypeName:e.target.value})}/>
      </div>
  </div>;
}
