import React from 'react';


export default function NewEntrySelector(props){
  let {onChangeAdditionSelection = e => console.log(e.target.value)} = props;
  return <div className='new-entry-selector'>
    <label>Select your addition: </label>
    <select onChange={e => onChangeAdditionSelection(e.target.value)}>
      <option value='exercise'>Exercise</option>
      <option value='exercise_type'>Exercise Type</option>
      <option value='exercise_category'>Exercise Category</option>
    </select>
  </div>;
}
