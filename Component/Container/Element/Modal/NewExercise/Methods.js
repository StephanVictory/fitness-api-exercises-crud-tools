function onChangeAdditionSelection(selectedAdditionType){
  console.log(selectedAdditionType);
  if(selectedAdditionType == 'exercise'){
    this.setState({newExerciseSelected:true,newExerciseTypeSelected:false,newExerciseCategorySelected:false});
    this.loadExerciseCategories();
  }else if(selectedAdditionType == 'exercise_type'){
    this.setState({newExerciseSelected:false,newExerciseTypeSelected:true,newExerciseCategorySelected:false});
  }else if(selectedAdditionType == 'exercise_category'){
    this.setState({newExerciseSelected:false,newExerciseTypeSelected:false,newExerciseCategorySelected:true});
  }
}

async function getExerciseCategories(){
  let exerciseCategories = [];
  let fetchConfig = { method : 'POST' };
  await fetch('http://localhost:8000/exerciseCategories/', fetchConfig)
    .then(response => exerciseCategories = response.json());
  return exerciseCategories;
}

async function loadExerciseCategories(){
  let exerciseCategories = await this.getExerciseCategories();
  this.setState({exerciseCategories:exerciseCategories});
}

async function addNewExercise(type, name){
  let result = {};
  await fetch('http://localhost:8000/addExercise/',
  {method:'POST',body: JSON.stringify({type:type,name:name})})
    .then(response => result = response.json())
    .catch(() => result = {result: 404, exerciseAddedAttempt:name});
  console.table(result);
}

async function addNewExerciseType(name){
  let result = {};
  await fetch('http://localhost:8000/addExerciseType/',
  {method:'POST',body: JSON.stringify({name:name})})
    .then(response => result = response.json())
    .catch(() => result = {result: 404, exerciseTypeAddedAttempt:name});
  console.table(result);
}

async function addNewExerciseCategory(name){
  let result = {};
  await fetch('http://localhost:8000/addExerciseCategory/',
  {method:'POST',body: JSON.stringify({name:name})})
    .then(response => result = response.json())
    .catch(() => result = {result: 404, exerciseCategoryAddedAttempt:name});
  console.table(result);
}

function handleNewEntrySubmit(e){
  if(this.state.newExerciseSelected){
    this.addNewExercise(this.state.newExerciseType,
      this.state.newExerciseName);
  }else if(this.state.newExerciseTypeSelected){
    this.addNewExerciseType(this.state.newExerciseTypeName);
  }else if(this.state.newExerciseCategorySelected){
    this.addNewExerciseCategory(this.state.newExerciseCategoryName);
  }
}



export default {
  onChangeAdditionSelection,
  getExerciseCategories,
  loadExerciseCategories,
  addNewExercise,
  addNewExerciseType,
  addNewExerciseCategory,
  handleNewEntrySubmit
};
