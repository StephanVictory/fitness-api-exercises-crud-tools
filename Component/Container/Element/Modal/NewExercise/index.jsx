import React from 'react';
import './Style/default.css';
import NewEntryPanel from './NewEntryPanel';
import NewEntrySelector from './NewEntrySelector';
import TitleUpperBar from './TitleUpperBar';

import Button from 'Container/Element/Button';
import Methods from './Methods';
import {autobindFunctions} from 'Utils/Helpers';

export default class NewExercise extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      exerciseCategories: [],
      newExerciseSelected:true,
      newExerciseTypeSelected:false,
      newExerciseCategorySelected:false,
      newExerciseType: -1,
      newExerciseName: '',
      newExerciseTypeName: '',
      newExerciseCategoryName: ''
    };
    autobindFunctions(this,Methods);
  }


  componentDidMount(){
    this.loadExerciseCategories();
  }

  render(){
    let visibilityStyleClass = this.props.isOpen ? 'modal-curtain':'modal-curtain-close';
    return <div className={`new-exercise-modal ${visibilityStyleClass}`}>
      <TitleUpperBar close={this.props.close}/>
        <div className="new-exercise-modal-content">
          <NewEntrySelector
            onChangeAdditionSelection={this.onChangeAdditionSelection}/>
          <NewEntryPanel
            newExerciseName={this.state.newExerciseName}
            newExerciseCategoryName={this.state.newExerciseCategoryName}
            newExerciseTypeName={this.state.newExerciseTypeName}
            setState={newState => this.setState(newState)}
            exerciseCategories={this.state.exerciseCategories}
            newExerciseTypeSelected={this.state.newExerciseTypeSelected}
            newExerciseCategorySelected={this.state.newExerciseCategorySelected}
            newExerciseSelected={this.state.newExerciseSelected}/>
            <div className='new-exercise-modal-action'>
              <Button text="Add Entry" size="md"
                onClick={this.handleNewEntrySubmit}/>
              <Button text="Cancel" color='crimson' size="md"
                onClick={this.props.close}/>
            </div>
        </div>

      </div>;
  }
}
