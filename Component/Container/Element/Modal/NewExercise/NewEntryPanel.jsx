import React from 'react';
import NewExercisePanel from './NewExercisePanel';
import NewExerciseTypePanel from './NewExerciseTypePanel';
import NewExerciseCategoryPanel from './NewExerciseCategoryPanel';

export default function NewEntryPanel(props){
  let {
    exerciseCategories = [],
    newExerciseSelected = true,
    newExerciseTypeSelected = false,
    newExerciseCategorySelected = false,
    newExerciseName,
    newExerciseTypeName,
    newExerciseCategoryName,
    setState
  } = props;

  if(newExerciseSelected){
    return <NewExercisePanel setState={setState}
      newExerciseName={newExerciseName} exerciseCategories={exerciseCategories}/>;
  }

  return newExerciseCategorySelected ?
  <NewExerciseCategoryPanel setState={setState}
    newExerciseCategoryName={newExerciseCategoryName}/> :
    <NewExerciseTypePanel setState={setState}
      newExerciseTypeName={newExerciseTypeName}/>;
}
