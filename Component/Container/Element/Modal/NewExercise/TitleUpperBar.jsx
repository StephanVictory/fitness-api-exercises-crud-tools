import React from 'react';
import CloseButton from 'Container/Element/Button/CloseButton';
export default function TitleUpperBar({close = () => console.log()}){
  return <div className="new-exercise-modal-upper-bar">
    <label className="new-exercise-modal-title">New Entry</label>
    <CloseButton classes='close-button-align-right' onClick={e => close()}/>
  </div>;
}
