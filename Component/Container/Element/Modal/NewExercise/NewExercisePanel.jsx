import React from 'react';


export default function NewExercisePanel(props){
  let {exerciseCategories = [], setState,newExerciseName = ''} = props;
  return  <div className='new-exercise-modal-entry'>
    <div>
      <label>Exercise name:</label>
      <input type='text' value={newExerciseName}
        onChange={e =>
          setState({newExerciseName:e.target.value})} />
    </div>
    <div>
      <label>Exercise Category:</label>
        <select onChange={e =>
            setState({newExerciseCategory:e.target.value})}>
          <option value='none'>---</option>
          {exerciseCategories.map(et =>
              <option key={et.value} value={et.value}>{et.name}</option>
            )
          }
        </select>
    </div>
  </div>;
}
