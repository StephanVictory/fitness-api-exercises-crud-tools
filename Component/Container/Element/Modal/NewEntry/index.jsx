import React from 'react';
import './Style/default.css';
import {autobindFunctions} from 'Utils/Helpers';
import Methods from './Methods';
import Selector from './Selector';
import Exercise from './Exercise';
import ExerciseType from './ExerciseType';
import ExerciseCategory from './ExerciseCategory';
import ExerciseDomain from './ExerciseDomain';
export default class NewEntry extends React.Component{
  constructor(props){
    super(props);
    this.newEntryComponents =
      {
        'exercise':Exercise,
        'exercise_type': ExerciseType,
        'exercise_category': ExerciseCategory,
        'exercise_domain': ExerciseDomain,
      };
    this.state =
      {
        selectedNewEntry: 'exercise',
      };
      autobindFunctions(this,Methods);
  }


  render(){
    let Entry =
      this.newEntryComponents[this.state.selectedNewEntry];
    return <div className='exercise'>
        <Selector onChange={this.handleOnChangeNewEntrySelection}/>
        <Entry {...this.props}/>
      </div>;
  }
}
