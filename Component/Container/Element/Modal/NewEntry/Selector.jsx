import React from 'react';

export default function Selector(props){
  let {onChange} = props;
  return <div className='new-entry-selector'>
    <label>Select your addition: </label>
    <select onChange={onChange}>
      <option value='exercise'>Exercise</option>
      <option value='exercise_type'>Exercise Type</option>
      <option value='exercise_category'>Exercise Category</option>
      <option value='exercise_domain'>Exercise Domain</option>
    </select>
  </div>;
}
