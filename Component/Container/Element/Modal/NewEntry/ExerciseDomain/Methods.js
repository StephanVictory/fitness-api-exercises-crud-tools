function handleOnChangeNewExerciseDomain(e){
  this.setState({name:e.target.value});
}

async function addNewExerciseDomain(){
  let submitInfo =
    {
      name:this.state.name
    };
  let result = null;
  let fetchConfig = {method:'POST', body: JSON.stringify(submitInfo)};
  await fetch('http://localhost:8000/addExerciseDomain/',fetchConfig)
    .then(async response => {result = await response.json();console.table(result);})
    .catch(reason => result = {result: 404, exerciseDomainAddedAttempt:name, reason: reason});
  return result;
}

async function submitNewEntry(){
  await this.addNewExerciseDomain();
}

export default {
  handleOnChangeNewExerciseDomain,
  addNewExerciseDomain,
  submitNewEntry
};
