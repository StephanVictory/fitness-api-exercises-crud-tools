function handleOnChangeNewExerciseCategory(e){
  this.setState({name:e.target.value});
}

async function addNewExerciseCategory(){
  let submitInfo =
    {
      name:this.state.name
    };
  let result = null;
  let fetchConfig = {method:'POST', body: JSON.stringify(submitInfo)};
  await fetch('http://localhost:8000/addExerciseCategory/',fetchConfig)
    .then(async response => {result = await response.json();console.table(result);})
    .catch(reason => result = {result: 404, exerciseCategoryAddedAttempt:name, reason: reason});
  return result;
}

async function submitNewEntry(){
  await this.addNewExerciseCategory();
}

export default {
  handleOnChangeNewExerciseCategory,
  addNewExerciseCategory,
  submitNewEntry
};
