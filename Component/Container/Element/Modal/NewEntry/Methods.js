function toggle(){
  this.props.toggle('NewEntry');
}

function submitNewEntry(){
   console.log('Submit new entry!');
}

function handleOnChangeNewEntrySelection(e){
  this.setState({selectedNewEntry:e.target.value});
}

export default {
  toggle,
  submitNewEntry,
  handleOnChangeNewEntrySelection
};
