import React from 'react';
import Button from 'Container/Element/Button';
export default function ButtonSection(props){
  let {submitNewEntry, toggle} = props;
  return <div className='btn-section'>
    <Button text='Add Entry' size='md'
      onClick={submitNewEntry}/>
    <Button text='Cancel' size='md' color='crimson'
      onClick={() => toggle('NewEntry')}/>
  </div>;
}
