import React from 'react';


export default function NameInput(props){
  let {name = '', handleOnChange} = props;
  return <div className='name-input'>
    <label>Exercise Type name:</label>
    <input type='text' value={name}
      onChange={handleOnChange}/>
  </div>
}
