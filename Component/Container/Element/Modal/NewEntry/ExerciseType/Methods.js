function handleOnChangeNewExerciseType(e){
  this.setState({name:e.target.value});
}

async function addNewExerciseType(){
  let submitInfo =
    {
      name:this.state.name
    };
  let result = null;
  let fetchConfig = {method:'POST', body: JSON.stringify(submitInfo)};
  await fetch('http://localhost:8000/addExerciseType/',fetchConfig)
    .then(async response => {result = await response.json();console.table(result);})
    .catch(reason => result = {result: 404, exerciseTypeAddedAttempt:name, reason: reason});
  return result;
}

async function submitNewEntry(){
  await this.addNewExerciseType();
}

export default {
  handleOnChangeNewExerciseType,
  addNewExerciseType,
  submitNewEntry
};
