import React from 'react';
import NameInput from './NameInput';
import ButtonSection from './ButtonSection';
import Methods from './Methods';
import {autobindFunctions} from 'Utils/Helpers';
export default class ExerciseType extends React.Component{
  constructor(props){
    super(props);
    this.state = {name: ''};
    autobindFunctions(this,Methods);
  }


  render(){
    return <React.Fragment>
      <div>
        <NameInput name={this.state.name}
          handleOnChange={this.handleOnChangeNewExerciseType}/>
      </div>
      <ButtonSection toggle={this.props.toggle}
        submitNewEntry={this.submitNewEntry}/>
    </React.Fragment>;
  }
}
