import React from 'react';

export default function CategorySelector(props){
  let {exerciseCategories,handleOnChange} = props;
  return <div className='category-selector'>
  <label>Exercise Category:</label>
      <select onChange={handleOnChange}>
        {exerciseCategories.map(et =>
          <option key={et.value} value={et.value}>{et.name}</option>
          )
        }
      </select>
    </div>
}
