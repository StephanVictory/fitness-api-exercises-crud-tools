import React from 'react';

export default function TypeSelector(props){
  let {exerciseTypes,handleOnChange} = props;
  return <div className='type-selector'>
  <label>Exercise Type:</label>
      <select onChange={handleOnChange}>
        {exerciseTypes.map(et =>
          <option key={et.value} value={et.value}>{et.name}</option>
          )
        }
      </select>
    </div>
}
