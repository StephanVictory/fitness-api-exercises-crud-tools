import React from 'react';
import {autobindFunctions} from 'Utils/Helpers';
import Methods from './Methods';
import NameInput from './NameInput';
import CategorySelector from './CategorySelector';
import TypeSelector from './TypeSelector';
import DomainSelector from './DomainSelector';
import ButtonSection from './ButtonSection';


export default class Exercise extends React.Component{
  constructor(props){
    super(props);
    this.state =
    {
      exerciseCategories: [],
      exerciseDomains: [],
      exerciseTypes: [],
      name:'',
      category:'',
      type:'',
      domain:'',
    };
    autobindFunctions(this,Methods);
  }

  componentDidMount(){
    this.loadExerciseCategories();
    this.loadExerciseTypes();
    this.loadExerciseDomains();
  }


  render(){
    return <React.Fragment>
        <div>
          <NameInput name={this.state.name}
            handleOnChange={this.handleOnChangeNewExercise}/>
          <CategorySelector
            exerciseCategories={this.state.exerciseCategories}
            handleOnChange={this.handleOnChangeNewExerciseCategory}/>
          <TypeSelector
            exerciseTypes={this.state.exerciseTypes}
            handleOnChange={this.handleOnChangeNewExerciseType}/>
          <DomainSelector
            exerciseDomains={this.state.exerciseDomains}
            handleOnChange={this.handleOnChangeNewExerciseDomain}/>
        </div>
        <ButtonSection toggle={this.props.toggle}
          submitNewEntry={this.submitNewEntry}/>
    </React.Fragment>;
  }
}
