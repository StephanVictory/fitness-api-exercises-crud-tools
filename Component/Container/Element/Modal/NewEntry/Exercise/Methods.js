async function loadExerciseCategories(){
  let fetchConfig = { method : 'POST' };
  let exerciseCategories = null;
  await fetch('http://localhost:8000/exerciseCategories/', fetchConfig)
    .then(async response => exerciseCategories = await response.json());
  this.setState({
      exerciseCategories: exerciseCategories,
      category: exerciseCategories[0].value
    });
}

async function loadExerciseTypes(){
  let fetchConfig = { method : 'POST' };
  let exerciseTypes = null;
  await fetch('http://localhost:8000/exerciseTypes/', fetchConfig)
    .then(async response => exerciseTypes = await response.json());
  this.setState({
    exerciseTypes: exerciseTypes,
    type: exerciseTypes[0].value
  });
}

async function loadExerciseDomains(){
  let fetchConfig = { method : 'POST' };
  let exerciseDomains = null;
  await fetch('http://localhost:8000/exerciseDomains/', fetchConfig)
    .then(async response => exerciseDomains = await response.json());
  this.setState({
    exerciseDomains: exerciseDomains,
    domain: exerciseDomains[0].value
  });
}

function handleOnChangeNewExercise(e){
  this.setState({name:e.target.value});
}

function handleOnChangeNewExerciseCategory(e){
  this.setState({category:e.target.value});
}

function handleOnChangeNewExerciseType(e){
  this.setState({type:e.target.value});
}

function handleOnChangeNewExerciseDomain(e){
  this.setState({domain:e.target.value});
}

async function addNewExercise(){
  let submitInfo =
    {
      type:this.state.type,
      name:this.state.name,
      category:this.state.category,
      domain: this.state.domain
    };
  let result = null;
  let fetchConfig = {method:'POST', body: JSON.stringify(submitInfo)};
  await fetch('http://localhost:8000/addExercise/',fetchConfig)
    .then(async response => {result = await response.json();console.table(result);})
    .catch(reason => result = {result: 404, exerciseAddedAttempt:name, reason: reason});
  return result;
}


async function submitNewEntry(){
  await this.addNewExercise();
}

export default {
  handleOnChangeNewExerciseCategory,
  handleOnChangeNewExerciseDomain,
  handleOnChangeNewExerciseType,
  handleOnChangeNewExercise,
  loadExerciseCategories,
  loadExerciseDomains,
  loadExerciseTypes,
  addNewExercise,
  submitNewEntry
};
