import React from 'react';
import 'Style/modal.css';
import ModalLibrary from 'Utils/ModalLibrary';
import CloseButton from 'Container/Element/Button/CloseButton';

function TitleBar({toggle, open,chosenModal,title}){
  return open ?
    <div>
      <label className='modal-title'>{title}</label>
      <CloseButton onClick={() => toggle(chosenModal)}/>
    </div> : null;
}

function ModalContent(props){
  let {open,chosenModal} = props;
  let ChosenModal = typeof ModalLibrary[chosenModal] !== 'undefined' ?
    ModalLibrary[chosenModal] : null;

  if(ChosenModal !== null){
    return <div className='modal-content'>
      {open ? <ChosenModal {...props}/>:null}
    </div> ;
  }
  
  return <div className='modal-content'>Modal not found</div>;
}

export default function Modal (props){
    let {chosenModal = ''} = props;
    return chosenModal !== '' ?
    <div className={`modal ${props.curtainMode}`}>
      <TitleBar {...props}/>
      <ModalContent {...props}/>
    </div> : null;
}
