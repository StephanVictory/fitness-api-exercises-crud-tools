import React from 'react';
import Methods from './Methods';
import {autobindFunctions} from 'Utils/Helpers';
import {Provider} from 'Store/Context/ModalContext';


export default function withModalContext(Component){
  return class extends React.Component{
    constructor(props){
      super(props);
      this.state =
        {
          open: false,
          curtainMode: 'modal-curtain-neutral',
          animationTimeout: null,
          chosenModal: '',
          title: ''
        };
      autobindFunctions(this,Methods);
    }

    render(){
      return (
        <Provider value={this}>
          <Component/>
        </Provider>
      );
    }
  };
}
