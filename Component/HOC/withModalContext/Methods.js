function toggle(chosenModal,title = ''){
  if(this.state.curtainMode === 'modal-curtain'){
    this.close(chosenModal);
  }else{
    this.open(chosenModal,title);
  }
}


function close(chosenModal){
  clearTimeout(this.state.animationTimeout);
  this.setState(
    {
      open: false,
      curtainMode: 'modal-curtain-close',
      animationTimeout:null,
      chosenModal: chosenModal
    }
  );
}


function open(chosenModal,title){
  this.setState(
    {
      curtainMode: 'modal-curtain',
      animationTimeout:setTimeout(this.fullyOpen,1000),
      chosenModal: chosenModal,
      title: title
    }
  );
}

function fullyOpen(){
  this.setState({open: true});
}

export default{
  open,
  close,
  toggle,
  fullyOpen
};
